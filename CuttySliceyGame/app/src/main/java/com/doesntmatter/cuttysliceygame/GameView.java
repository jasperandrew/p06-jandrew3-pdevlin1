package com.doesntmatter.cuttysliceygame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This is the game view, and the game loop code is in here.
 * I've added some comments to try to explain the important stuff.
 */

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;
    private Fruit fruit;

    /**
     * Basically this is where you initialize all the pieces of the game.
     */
    public GameView(Context context) {
        super(context);
        getHolder().addCallback(this);

        thread = new MainThread(getHolder(), this);

        fruit = new Fruit(Const.SCREEN_WIDTH/2-50, Const.SCREEN_HEIGHT, 100, 110, 0, 30, context);
        fruit.init();

        setFocusable(true);
    }

    /**
     * This is run every loop, so any elements that need to update their data
     * while the game is running need to be called from here.
     */
    public void update() {
        if(!Const.GAME_OVER) {
            fruit.update();
        }
    }

    /**
     * This is similar to update, but pertains to the visual state of elements.
     * It draws the updated state to the canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if(!Const.GAME_OVER){ // Check if game is over
            canvas.drawColor(Color.rgb(222,184,135)); // Background color of canvas
            fruit.draw(canvas);
        }else{
            canvas.drawColor(Color.rgb(222,184,135)); // Background color of canvas
            GameOverScreen gameOverScreen = new GameOverScreen();
            gameOverScreen.draw(canvas);
        }
    }

    /**
     * This is just for resetting the game state when the player loses or whatever.
     */
    private void resetGame() {
        Const.GAME_OVER = false;
        fruit.init();
    }

    /**
     * This is for handling touch events. I've only ever used the basic down-touch event,
     * but I'm sure it's not hard to use others.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                if(Const.GAME_OVER){
                    resetGame();
                    return true;
                }
        }
        return true;
    }

    /**
     * Everything below here I just took from the original tutorial I watched.
     * I've never changed any of it, and I'm not 100% sure what it does.
     * I'm guessing it handles what happens when the gameview is initialized on the device or something.
     */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread = new MainThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while(retry){
            try {
                thread.setRunning(false);
                thread.join();
            } catch(Exception e) { e.printStackTrace(); }

            retry = false;
        }
    }

}