package com.doesntmatter.cuttysliceygame;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

class GameOverScreen {
    private Paint textPaint;

    GameOverScreen() {
        textPaint = new Paint();

        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(150);
        textPaint.setFakeBoldText(true);
    }

    void draw(Canvas canvas) {
        canvas.drawText("CONTEMPLATE", Const.SCREEN_WIDTH/2, Const.SCREEN_HEIGHT/2-200, textPaint);
        canvas.drawText("GRAVITY", Const.SCREEN_WIDTH/2, Const.SCREEN_HEIGHT/2+200, textPaint);
    }
}
