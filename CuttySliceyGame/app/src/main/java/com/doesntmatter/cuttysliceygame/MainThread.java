package com.doesntmatter.cuttysliceygame;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * This is the main thread of the game. I rarely, if ever, change any of this.
 * I kinda know what it does, but I haven't messed with it enough to be comfortable changing stuff here.
 * You can feel free to mess with stuff if you want to figure it out, just make sure it works after, lol.
 */

class MainThread extends Thread {
    private final SurfaceHolder surfaceHolder;
    private GameView gameView;
    private boolean running;



    void setRunning(boolean running) {
        this.running = running;
    }
    MainThread(SurfaceHolder surfaceHolder, GameView gameView) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.gameView = gameView;
    }

    @Override
    public void run() {
        long startTime;
        long timeMillis;
        long waitTime;
        int frameCount = 0;
        long targetTime = 1000/Const.MAX_FPS;

        while(running){
            startTime = System.nanoTime();
            Canvas canvas = null;

            try {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder){
                    this.gameView.update();
                    this.gameView.draw(canvas);
                }
            } catch(Exception e){ e.printStackTrace(); }
            finally {
                if(canvas != null){
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch(Exception e) { e.printStackTrace(); }
                }
            }

            timeMillis = (System.nanoTime() - startTime)/1000000;
            waitTime = targetTime - timeMillis;

            try {
                if(waitTime > 0) sleep(waitTime);
            } catch (Exception e){ e.printStackTrace(); }

            frameCount++;

            if(frameCount == Const.MAX_FPS){
                frameCount = 0;
            }
        }
    }
}

