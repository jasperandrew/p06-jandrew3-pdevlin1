package com.doesntmatter.cuttysliceygame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

class Fruit {

    private Rect rect;
    private int width, startX;
    private int height, startY;
    private float hAccel;
    private float vAccel;
    private float initAccel;
    private int direction;
    private Bitmap img;

    Fruit(int startX, int startY, int width, int height, float hAccel, float initAccel, Context context) {
        this.width = width;
        this.height = height;
        this.startX = startX;
        this.startY = startY;
        this.hAccel = hAccel;
        this.vAccel = initAccel;
        this.initAccel = initAccel;
        rect = new Rect(startX, startY, startX+width, startY+height);
        img = BitmapFactory.decodeResource(context.getResources(), R.drawable.apple);
    }

    void init() {
        vAccel = initAccel;
        rect.top = startY-height/2;
        rect.bottom = startY+height/2;
    }

    void draw(Canvas canvas) {
        canvas.drawBitmap(img, null, rect, new Paint());
    }

    void update() {
        rect.left += hAccel;
        rect.right += hAccel;
        vAccel -= Const.GRAVITY;
        rect.top -= vAccel;
        rect.bottom -= vAccel;

        if(rect.top > Const.SCREEN_HEIGHT){
            Const.GAME_OVER = true;
        }
    }
}
