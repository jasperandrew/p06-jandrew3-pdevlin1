package com.doesntmatter.cuttysliceygame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import java.util.ArrayList;

class FruitFlinger {
    private ArrayList<Fruit> fruits;
    private ArrayList<Bitmap> imgs;
    private int launch_rate, srcX, srcY;
    private final int apple_w = 100, apple_h = 110, banana_w = 300, banana_h = 100;

    FruitFlinger(int launch_rate, int srcX, int srcY, boolean left_side, Context context) {
        fruits = new ArrayList<>();
        imgs = new ArrayList<>();

        this.launch_rate = launch_rate;
        this.srcX = srcX;
        this.srcY = srcY;

        imgs.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.apple));
        imgs.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.banana));
    }

    void init() {
        //fruits.add(new Fruit(srcX-apple_w/2, srcY-apple_h/2, ));
    }

    void draw(Canvas canvas) {
        for(Fruit fruit : fruits){
            fruit.draw(canvas);
        }
    }

    void update() {
        for(Fruit fruit : fruits){
            fruit.update();
        }
    }
}