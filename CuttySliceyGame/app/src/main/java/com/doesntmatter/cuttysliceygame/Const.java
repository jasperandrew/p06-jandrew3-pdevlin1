package com.doesntmatter.cuttysliceygame;

import android.content.Context;

/**
 * This is just a global constants file I use. You access anything in here by putting Const.SCORE or whatever.
 * Easier than creating and updating individual objects.
 */

class Const {
    static int SCREEN_WIDTH;
    static int SCREEN_HEIGHT;
    static final int MAX_FPS = 60;
    public static float GRAVITY = 0.5f;
    static int SCORE;
    static boolean GAME_OVER = false;
}